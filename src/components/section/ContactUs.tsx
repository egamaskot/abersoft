import React from 'react';

const ContactUs = () => {
    return (
        <div className="mt-14 grid grid-cols-1 sm:grid-cols-2 gap-8">
            <div className="flex flex-col justify-between h-full pb-[100px]">
                <div>
                    <h2 className="text-4xl font-bold mb-4 text-[#0E97FD]">
                        Contact Us
                    </h2>
                    <p className="mb-4 text-[#707070] mr-20" >
                    We know that it sometimes can be hard to know where to start. Let’s chat and see if we can help you!
                    </p>
                </div>
                <div className="flex space-x-4 mb-6">
                    <a href="#" className="text-blue-500 hover:text-blue-700">
                        <img src="/icon/Button_Facebook.png" alt="Facebook Icon" />
                    </a>
                    <a href="#" className="text-blue-500 hover:text-blue-700">
                        <img src="/icon/Button_Instagram.png" alt="Instagram Icon" />
                    </a>
                    <a href="#" className="text-blue-500 hover:text-blue-700">
                        <img src="/icon/Button_LinkedIn.png" alt="LinkedIn Icon" />
                    </a>
                </div>
            </div>


            <div>
                <form className=" rounded-md">
                    <div className="mb-4">
                        <input
                            type="text"
                            id="name"
                            name="name"
                            placeholder="Name"
                            className="w-full bg-[#F6F6F9] border-gray-300 focus:ring focus:ring-[#0E97FD] text-[#707070] focus:border-[#0E97FD] p-5 rounded-[29.4px]"
                        />
                    </div>
                    <div className="mb-4">
                        <input
                            type="email"
                            id="email"
                            name="email"
                            placeholder="Email"
                            className="w-full bg-[#F6F6F9] border-gray-300 focus:ring focus:ring-[#0E97FD] text-[#707070] focus:border-[#0E97FD] p-5 rounded-[29.4px]"
                        />
                    </div>
                    <div className="mb-4">
                        <textarea
                            id="message"
                            name="message"
                            rows={6}
                            placeholder="Message"
                            className="w-full bg-[#F6F6F9] border-gray-300 focus:ring focus:ring-[#0E97FD] text-[#707070] focus:border-[#0E97FD] p-5 rounded-[29.4px]"
                        />
                    </div>
                    <div className="text-right">
                        <button
                            type="submit"
                            className="bg-[#0E97FD] text-white font-medium py-4 px-16 hover:bg-blue-700 rounded-[29.4px] my-5"
                        >
                            Send
                        </button>
                    </div>
                </form>
            </div>
        </div>
    );
};

export default ContactUs;
