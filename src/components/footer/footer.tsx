import React from 'react';

const Footer = () => {
    return (
        <footer className="bg-[#0E97FD] bottom-0 sticky text-white py-8">
        <div className="w-full container mx-auto flex flex-wrap justify-center max-w-[1440px] px-[6%]">
            <div className="w-full sm:w-1/2 md:w-1/4 lg:w-1/4 xl:w-1/4 mb-6 sm:mb-0">
            <h3 className="text-lg font-semibold mb-4">Our Services</h3>
            <ul className="list-none">
                <li className="mb-2"><a href="#">App Development</a></li>
                <li className="mb-2"><a href="#">Websites</a></li>
                <li className="mb-2"><a href="#">UI/UX Design</a></li>
                <li className="mb-2"><a href="#">Ongoing Consultation</a></li>
            </ul>
            </div>

            <div className="w-full sm:w-1/2 md:w-1/4 lg:w-1/4 xl:w-1/4 mb-6 sm:mb-0">
            <h3 className="text-lg font-semibold mb-4">Documents</h3>
            <ul className="list-none">
                <li className="mb-2"><a href="/term-condition">Terms & Conditions</a></li>
                <li className="mb-2"><a href="/privacy-policy">Privacy Policy</a></li>
            </ul>
            </div>

            <div className="w-full sm:w-1/2 md:w-1/4 lg:w-1/4 xl:w-1/4 mb-6 sm:mb-0">
            <h3 className="text-lg font-semibold mb-4">Inhouse</h3>
            <ul className="list-none">
                <li className="mb-2"><a href="#">Haunted House AR</a></li>
                <li className="mb-2"><a href="#">Abersoft Blog</a></li>
                <li className="mb-2"><a href="/workwithus">Work With Us</a></li>
            </ul>
            </div>

            <div className="w-full sm:w-1/2 md:w-1/4 lg:w-1/4 xl:w-1/4 mb-6 sm:mb-0 text-right justify-end items-end">
                <h3 className="text-lg font-semibold mb-4">Address</h3>
                <p>Kometvägen 3</p>
                <p>183 33 TÄBY</p>
                <p>Stockholm, Sweden</p>
                <br/>
                <p>Kampung Yadika Regency</p>
                <p>Blok H Nomer 4</p>
                <p>Pasuruan, Indonesia</p>
                <br/>
                <img
                    src="/icon/logo.png"
                    width="150px"
                    style = {{float: 'right'}}
                />
                </div>
        </div>
        </footer>
    );
};

export default Footer;
