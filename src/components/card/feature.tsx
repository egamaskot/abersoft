import React from 'react';

interface FeatureProps {
    image: string;
    title: string;
    description: string;
}

const Feature = ({ image, title, description } : FeatureProps) => {
    return (
        <div className="w-[90%] bg-white rounded-lg overflow-hidden mt-[60px]">
            <img 
                src={image} 
                alt={title} 
                className={`w-[70%] justify-center items-center object-cover object-center mx-auto`}
            />
            <div className="px-10">
                <h2 className="text-xl font-semibold text-[#0E97FD]">{title}</h2>
                <p className="text-gray-600">{description}</p>
            </div>
        </div>
    );
};

export default Feature;
