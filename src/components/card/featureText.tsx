import React from 'react';

interface FeatureTextProps {
    title: string;
    subtitle: string;
    description: any;
}

const FeatureText = ({ title, subtitle, description }: FeatureTextProps) => {
    return (
        <div className="w-[90%] bg-white rounded-lg overflow-hidden mt-[30px]">
            <div className="px-10 py-6">
                <h2 className="text-xl font-semibold text-[#0E97FD]">{title}</h2>
                <h3 className="mt-4 font-normal text-[#707070]">{subtitle}</h3>
                <p className="mt-4 font-light text-[#707070]" dangerouslySetInnerHTML={{ __html: description.replace(/\n/g, '<br/>') }}/>
            </div>
        </div>
    );
};

export default FeatureText;
