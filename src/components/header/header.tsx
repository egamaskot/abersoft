import { useRouter, usePathname } from 'next/navigation';
import Link from 'next/link';

const Header: React.FC = () => {
    const router = useRouter();
    const pathname = usePathname();

    const tabs = [
        { path: '/home', label: 'Home' },
        { path: '/services', label: 'Services' },
        { path: '/about', label: 'About us' },
        { path: '/references', label: 'References' },
        { path: '/contact', label: 'Contact' },
    ];

    return (
        <header className="py-[24px] w-full fixed top-0 left-0 right-0 bg-white z-50 items-center justify-center mx-auto">
        <nav className='max-w-[1440px] items-center justify-center mx-auto'>
            <ul 
                className="flex justify-around items-center p-4"
            >
            {tabs.map((tab) => (
                <li
                key={tab.path}
                className={pathname === tab.path ? 'text-blue-600 font-bold' : ''}
                >
                <Link href={tab.path}>
                    <p 
                        className={pathname === tab.path ? 'text-0E97FD' : 'text-black' }
                    >{tab.label}</p>
                </Link>
                </li>
            ))}
            </ul>
        </nav>
        </header>
    );
};

export default Header;
