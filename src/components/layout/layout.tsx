import React, { ReactNode } from 'react';
import Header from '../header/header';
import Footer from '../footer/footer';

interface LayoutProps {
    children: ReactNode;
}

const Layout = ({ children } : LayoutProps) => {
    return (
        <div className=" inset-0 flex flex-col justify-center z-50 w-full overflow-x-hidden">
            <Header/>
            <main className="mt-14 max-w-[1440px] w-full flex flex-col mx-auto px-4">
                <div className="mt-14 w-full max-w-[1440px]" >
                    {children}
                </div>
            </main>
            <Footer/>
        </div>
    );
};

export default Layout;
