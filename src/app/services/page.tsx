"use client";

import { useEffect, useState } from 'react';
import Layout from 'abersoft/components/layout/layout';
import Feature from 'abersoft/components/card/feature';
import ContactUs from 'abersoft/components/section/ContactUs';
import FeatureText from 'abersoft/components/card/featureText';

export default function Services() {
  const [isVisible, setIsVisible] = useState(true);

  useEffect(() => {
    const timer = setTimeout(() => {
      setIsVisible(false);
    }, 3000);

    return () => clearTimeout(timer);
  }, []);

  return (
    <Layout>
      <div className=' mx-[7%]'>
        <div className="w-full flex flex-row pt-[175px]">
          <div className="w-1/2 pr-[50px]">
            <h1 className="text-[#0E97FD] text-[48px] font-bold">
            We make your ideas become <span className="text-[#707070]">reality.</span>
            </h1>
            <p className="text-[#707070] pr-10">
            Whether or not you are an established company or a new start up, we will create the solution you need.
            </p>
          </div>
          <div className="w-1/2 justify-center mx-auto">
            <img
              src="/image/Image_Teddy.png"
              className="mt-[-200px] scale-80"
            />
          </div>
        </div>
        <div className=" mb-[80px] grid grid-cols-1 sm:grid-cols-2 md:grid-cols-2 gap-4">
          <FeatureText 
            title="App Development"
            subtitle='With over 10 years of experience of creating apps on both Android and iOS we can create the perfect app for you.'
            description="We have built app portfolios with over 400 million downloads world wide ranging from AR Games to complicated business apps. We always strive to build apps with the latest technology and that will be easy for our clients to manage and handle in the future. We use frameworks such as React Native and Flutter to create amazing cross platform applications."
          />
          <FeatureText 
            title="Websites"
            subtitle='Having a professional website can sometimes be the difference between getting a new client or not.'
            description="With long experience of creating websites both from scratch and using many frameworks such as Wordpress, WooCommerce and other platforms we can create the perfect website for you. We always try making the website as smooth and fast as possible with the right SEO to help you get found online."
          />
          <FeatureText 
            title="UI/UX Design"
            subtitle='If your tech is like the brain, design is like the heart. In order to get clients to fall in love with your product you need to have great design in place.'
            description="We know design can be tricky so we are here to assist you creating beautiful UI/UX Design following the latest trends and designs. We always use industry standard softwares such as Figma, Adobe and Photoshop."
          />
          <FeatureText 
            title="Consultant"
            subtitle='For already established companies we offer ongoing consultants at unbeatable prices.'
            description="Sometimes you do not need or want to employ a full time worker because it is more flexible with an ongoing consultant. We can offer a wide variety of services ranging from really cost effective remote consultants with Swedish project management to on site Swedish developers."
          />
        </div>
        <div className="mt-[190px] mb-5">
          <ContactUs/>
        </div>
      </div>
  </Layout>
  );
}
