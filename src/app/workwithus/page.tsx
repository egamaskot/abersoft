"use client";

import { useEffect, useState } from 'react';
import Layout from 'abersoft/components/layout/layout';
import FeatureText from 'abersoft/components/card/featureText';
import ContactUs from 'abersoft/components/section/ContactUs';

export default function About() {
  const [isVisible, setIsVisible] = useState(true);

  useEffect(() => {
    const timer = setTimeout(() => {
      setIsVisible(false);
    }, 3000);

    return () => clearTimeout(timer);
  }, []);

  return (
    <Layout>
      <div className=' mx-[7%]'>
        <div className="w-full flex flex-row pt-[175px]">
          <div className="w-1/2 pr-[50px]">
            <h1 className="text-[#0E97FD] text-[48px] font-bold">
            Become part of our great team.
            </h1>
            <p className="text-[#707070] pr-10">
            We are always looking for new great talents, if you want to be part of a growing team that have fun work.
            </p>
          </div>
          <div className="w-1/2 justify-center mx-auto">
            <img
              src="/image/hero-8.png"
              className="mt-[-100px] ml-[150px] scale-80"
            />
          </div>
        </div>
        <div className="mt-[190px] mb-5">
          <ContactUs/>
        </div>
      </div>
  </Layout>
  );
}
