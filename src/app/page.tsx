"use client";

import { useEffect, useState } from 'react';
import styles from './page.module.scss';
import { useRouter } from 'next/navigation';
import Layout from 'abersoft/components/layout/layout';

export default function SplashScreen() {
  const [isVisible, setIsVisible] = useState(true);
  const [isHome, setIsHome] = useState(true)
  const router = useRouter();

  useEffect(() => {
    const hideSplashScreen = () => {
      setIsVisible(false);
    };

    const redirectToHomePage = () => {
      setIsHome(false)
      router.push("/home")
    };

    const hideTimer = setTimeout(hideSplashScreen, 2000);
    const redirectTimer = setTimeout(redirectToHomePage, 3000);

    return () => {
      clearTimeout(hideTimer);
      clearTimeout(redirectTimer);
    };
  }, [router]);

  return (
    <>
        <div
          className={`${styles.background} fixed h-[100vh] inset-0 flex justify-center items-center z-50 ${
            isHome ? 'opacity-100' : 'opacity-0'
          }`}
        >
          <img
            src="/icon/logo.png"
            alt="Abersoft Logo"
            className={isVisible ? styles.fadeIn : styles.fadeOut}
          />
        </div>
    </>
  );
}
