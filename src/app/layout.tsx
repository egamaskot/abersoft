import './globals.css';
import type { Metadata } from 'next'
import { Inter } from 'next/font/google'
import Head from 'next/head';
import Logo from '/public/logo.png';

const inter = Inter({ subsets: ['latin'] })

export const metadata: Metadata = {
  title: 'Abersoft',
  description: 'Abersoft',
  icons: { 
    icon: '/logo.png' 
  }
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <head>
      <link rel="icon" href="/logo.png" sizes="192x192" />
        <link rel="icon" href="/logo.png" sizes="128x128" />
        <link rel="icon" href="/logo.png" sizes="100x100" />
        <link rel="icon" href="/logo.png" sizes="64x64" />
        <link rel="icon" href="/logo.png" sizes="32x32" />
        <link rel="icon" href="/logo.png" sizes="128x128" />
        <link rel="icon" href="/logo.png" type="image/png"/>
      </head>
      <body className={inter.className}>{children}</body>
    </html>
  )
}
