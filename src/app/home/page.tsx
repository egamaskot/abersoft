"use client";

import { useEffect, useState } from 'react';
import Layout from 'abersoft/components/layout/layout';
import Feature from 'abersoft/components/card/feature';
import ContactUs from 'abersoft/components/section/ContactUs';

export default function Home() {
  const [isVisible, setIsVisible] = useState(true);

  useEffect(() => {
    const timer = setTimeout(() => {
      setIsVisible(false);
    }, 3000);

    return () => clearTimeout(timer);
  }, []);

  return (
    <Layout>
      <div className=' mx-[7%]'>
        <div className="w-full flex flex-row pt-[175px]">
          <div className="w-1/2 pr-[50px]">
            <h1 className="text-[#0E97FD] text-[48px] font-bold">
            We help you build the tech solutions of the future.
            </h1>
            <p className="text-[#707070] pr-10">
            When you need help with development or design we are here to create the best solutions for you. With over a decade of experience in software development.
            </p>
          </div>
          <div className="w-1/2 justify-center mx-auto scale-120">
            <img
              src="/image/Image_Phone.png"
            />
          </div>
        </div>
        <div className="mt-[110px] mb-[80px] grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 gap-4">
          <Feature 
            image="/icon/Other04.png"
            title="App Development"
            description="We help you build native and cross-platform apps. We have helped clients reach Top Grossing apps with over 400 million downloads world wide."
          />
          <Feature 
            image="/icon/Other10.png"
            title="Websites"
            description="We work with all the most common websites systems such as Wordpress och WooCommerce. But we also build websites from scratch for you."
          />
          <Feature 
            image="/icon/Other02.png"
            title="MVP & Prototype"
            description="If you have an MVP that you need help getting developed or an idea of a prototype, let us know and we will assist you with the whole process."
          />
          <Feature 
            image="/icon/Other20.png"
            title="UI/UX Design"
            description="Besides development we also have a great team of UI/UX designers that will take your old design to a whole new level or create new one from scratch."
          />
          <Feature 
            image="/icon/Other18.png"
            title="Consultant"
            description="We can also provide you with an ongoing consultant that will be a great addition to your already established team."
          />
          <Feature 
            image="/icon/Other07.png"
            title="Software System"
            description="We work with all kinds of software technologies such as Unity, Strapi & other useful integrations that will help you and your company."
          />
        </div>
        <div className="mt-[190px] mb-5">
          <ContactUs/>
        </div>
      </div>
  </Layout>
  );
}
