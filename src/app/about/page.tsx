"use client";

import { useEffect, useState } from 'react';
import Layout from 'abersoft/components/layout/layout';
import FeatureText from 'abersoft/components/card/featureText';
import ContactUs from 'abersoft/components/section/ContactUs';

export default function About() {
  const [isVisible, setIsVisible] = useState(true);

  useEffect(() => {
    const timer = setTimeout(() => {
      setIsVisible(false);
    }, 3000);

    return () => clearTimeout(timer);
  }, []);

  return (
    <Layout>
      <div className=' mx-[7%]'>
        <div className="w-full flex flex-row pt-[175px]">
          <div className="w-1/2 pr-[50px]">
            <h1 className="text-[#707070] text-[48px] font-bold">
            Who are we? <br/> We are <span className="text-[#0E97FD]">Abersoft.</span>
            </h1>
            <p className="text-[#707070] pr-10">
            We have been working with software development since 2013 and our team has created apps with over 400 million users.
            </p>
          </div>
          <div className="w-1/2 justify-center mx-auto">
            <img
              src="/image/Image_People.png"
              className="mt-[-100px] ml-[150px] scale-80"
            />
          </div>
        </div>
        <div className="bg-[#0E97FD] mb-[80px] mx-[-100%] mt-[200px] flex justify-center">
          <div className="text-center text-white justify-center items-center py-20">
            <img
              src="/icon/quote-left.png"
              className="mx-[45%] my-6"
            />
            <h1 className="text-[42px] font-semibold">Creating Software</h1>
            <h1 className="text-[42px] font-semibold">is not our work,</h1>
            <h1 className="text-[42px] font-semibold">it is our passion.</h1>
            <img
              src="/icon/quote-right.png"
              className="mx-[45%] my-6"
            />
          </div>
        </div>
        <div className="w-full flex flex-row relative">
          <div className="w-1/2 pr-[50px]">
            <FeatureText
              title='Our philosophy.'
              subtitle='Our mission is to make our clients life as smooth and problem free as possible and deliver top products.'
              description={`We have been in the tech industry over a decade and know that it can sometimes be a jungle for someone who is unfamilliar with it. We are here to assist you and make it fun and easy for you to work. \n \n We have spent the past years in putting together an international team and workflow so we can optimize cost, work fast and deliver great results. \n\n 2019 we won the Innovation & Entrepreneurship Award in Kunming for our work.`}
            />
          </div>
          <div className="w-1/2 justify-center mx-auto">
            <img
              src="/image/Image_Hand.png"
              className="ml-[150px] scale-80 mb-[-150px] z-10"
            />
          </div>
        </div>
        <div className="bg-[#0E97FD] mb-[80px] mx-[-100%] flex justify-center relative">
          <div className="text-center text-white justify-center items-center py-40">
            <h1 className="text-[42px] font-semibold">Abersoft,</h1>
            <h1 className="text-[42px] font-semibold">more than just a</h1>
            <h1 className="text-[42px] font-semibold">software house.</h1>
          </div>
        </div>
        <div className="w-full flex flex-row relative items-center">
          <div className="w-1/2 pr-[50px]">
            <FeatureText
              title='Game Studio & Inhouse Apps.'
              subtitle=''
              description={`Besides helping companies and start ups to create awesome apps and websites we also have our own inhouse branch where we explore new technologies that we find interesting. \n\n Lately we have been focusing on exploring the field of Augmented Reality, more commonly known as AR. Besides AR we also love building software that makes life easier for our users. \n\n Are you looking for a tech partner for your idea, we sometimes go in as part owners in ideas we feel passionate about. Contact us if you have an idea that you think we might be interested in.`}
            />
          </div>
          <div className="w-1/2 justify-center mx-auto">
            <img
              src="/image/Image_Game.png"
              className="ml-[150px] mt-20 scale-80 z-10"
            />
          </div>
        </div>
        <div className="mt-[190px] mb-5">
          <ContactUs/>
        </div>
      </div>
  </Layout>
  );
}
