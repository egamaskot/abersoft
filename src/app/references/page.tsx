"use client";

import { useEffect, useState } from 'react';
import Layout from 'abersoft/components/layout/layout';

export default function References() {
  const [isVisible, setIsVisible] = useState(true);

  useEffect(() => {
    const timer = setTimeout(() => {
      setIsVisible(false);
    }, 3000);

    return () => clearTimeout(timer);
  }, []);

  return (
    <Layout>
    <div>
      <p className="text-black h-[50vh] text-center">
        this is References
      </p>
    </div>
  </Layout>
  );
}
